﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour {

    public GameObject gameButtonPrefab;

    public List<ButtonSetting> buttonSettings;
    public Transform gameFieldPanelTransform;
    public int bleepCount = 1;
    public TextMeshProUGUI score, scoreAtEnd;
    public GameObject panelEnd, canvasGameplay;
    
    private List<int> _bleeps;
    private List<int> _playerBleeps;
    private List<GameObject> _gameButtons;
    
    private System.Random _rg;
    private int _randomNumberGenerator;
    
    private bool _inputEnabled = false;
    private bool _gameOver = false;

    void Start() // Crée les boutons et choisis un nombre et une séquence aléatoire
    {
        _gameButtons = new List<GameObject>();
        
        panelEnd.SetActive(false);
        
        CreateGameButton(0, new Vector3(0, 180));
        CreateGameButton(1, new Vector3(0, 90));
        CreateGameButton(2, new Vector3(0, 0));
        CreateGameButton(3, new Vector3(0, -90));
        CreateGameButton(4, new Vector3(0, -180));
        _randomNumberGenerator =  Random.Range(0, 1000000000);
        StartCoroutine(SimonSays());
    }

    void CreateGameButton(int index, Vector3 position) // Instantie les boutons et les ajoute à la liste
    {
        GameObject gameButton = Instantiate(gameButtonPrefab, Vector3.zero, Quaternion.identity) as GameObject;

        gameButton.transform.SetParent(gameFieldPanelTransform);
        gameButton.transform.localPosition = position;

        gameButton.GetComponent<Image>().color = buttonSettings[index].normalColor;
        gameButton.GetComponent<Button>().onClick.AddListener(() => {
            OnGameButtonClick(index);
        });

        _gameButtons.Add(gameButton);
    }

    void PlayAudio(int index) // Lance le son quand on clique sur un bouton
    {
        float length = 0.5f;
        float frequency = 0.001f * ((float)index + 1f);

        AnimationCurve volumeCurve = new AnimationCurve(new Keyframe(0f, 1f, 0f, -1f), new Keyframe(length, 0f, -1f, 0f));
        AnimationCurve frequencyCurve = new AnimationCurve(new Keyframe(0f, frequency, 0f, 0f), new Keyframe(length, frequency, 0f, 0f));

        LeanAudioOptions audioOptions = LeanAudio.options();
        audioOptions.setWaveSine();
        audioOptions.setFrequency(44100);

        AudioClip audioClip = LeanAudio.createAudio(volumeCurve, frequencyCurve, audioOptions);

        LeanAudio.play(audioClip, 0.5f);
    }

    void OnGameButtonClick(int index) // Ajoute 1 à la liste quand on clique sur un bouton et regarde si c'est le bon bouton
    {
        if(!_inputEnabled) {
            return;
        }

        Bleep(index);

        _playerBleeps.Add(index);

        if(_bleeps[_playerBleeps.Count - 1] != index) {
            GameOver();
            return;
        }

        if(_bleeps.Count == _playerBleeps.Count) {
            StartCoroutine(SimonSays());
        }
    }

    void GameOver() // Quand la partie est terminée
    {
        _gameOver = true;
        _inputEnabled = false;
        panelEnd.SetActive(true);
        canvasGameplay.SetActive(false);
    }

    IEnumerator SimonSays() // S'occupe de l'aléatoire de la séquence
    {
        _inputEnabled = false;
        var randomNumber = _randomNumberGenerator;
        _rg = new System.Random(randomNumber.GetHashCode());

        SetBleeps();

        yield return new WaitForSeconds(1.5f);

        for(int i = 0; i < _bleeps.Count; i++) {
            Bleep(_bleeps[i]);

            yield return new WaitForSeconds(0.6f);
        }

        _inputEnabled = true;

        yield return null;
    }

    void Bleep(int index) // Comment réagis le bouton
    {
        LeanTween.value(_gameButtons[index], buttonSettings[index].normalColor, buttonSettings[index].highlightColor, 0.25f).setOnUpdate((Color color) => {
            _gameButtons[index].GetComponent<Image>().color = color;
        });

        LeanTween.value(_gameButtons[index], buttonSettings[index].highlightColor, buttonSettings[index].normalColor, 0.25f)
            .setDelay(0.5f)
            .setOnUpdate((Color color) => {
                _gameButtons[index].GetComponent<Image>().color = color;
            });

        PlayAudio(index);
    }

    void SetBleeps() // Permet de reprendre la liste déjà utilisée pour rejouer les mêmes boutons et d'en ajouter un. 
    { 
        _bleeps = new List<int>();
        _playerBleeps = new List<int>();

        for(int i = 0; i < bleepCount; i++) {
            _bleeps.Add(_rg.Next(0, _gameButtons.Count));
        }
        score.text = bleepCount.ToString();
        scoreAtEnd.text = score.text;
        bleepCount++;
    }
}