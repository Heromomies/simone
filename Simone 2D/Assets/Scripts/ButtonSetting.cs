﻿using System;
using UnityEngine;

[Serializable]
public class ButtonSetting 
{
    public Color normalColor;
    public Color highlightColor;
}
